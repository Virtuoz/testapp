class Post < ActiveRecord::Base
	validates :title, presence: true, length: { minimum: 5 }
	validates :title, presence: true, length: { maximum: 300 }
	validates :user_id, presence: true
	belongs_to :user
	belongs_to :status
end
